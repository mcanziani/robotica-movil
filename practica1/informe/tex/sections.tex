%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Trabajo Práctico en formato paper
% Template LaTeX
% Version 2 (octubre 2014)
%
% Basado en:
% Template original de
% http://www.LaTeXTemplates.com
% Por:
% Frits Wenneker (http://www.howtotex.com)
% Editado por Mariela J. (FCEyN UBA) 04/02/2014
% Adaptado para Trabajos Prácticos por Martín C. (FI UBA) 04/10/2014
% Modularización según 
%  https://en.wikibooks.org/wiki/LaTeX/Modular_Documents
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Pra referenciar una cita bibliográfica, la llaman por \cite[p.~215]{kailath} y \cite{reyvega}.
%\begin{figure}[H]
%  \centerline{
%    \includegraphics[width=0.5\textwidth]{./img/mixer}
%    }
%  \caption{\footnotesize {Diagrama de señales en un mixer\cite{pres}}}
%  \label{fig:mixer}
%\end{figure}
%Matriz de cambio de base de b > e
%$$C_{b}^{e}(t)=\left[\begin{array}{cc}
%z_1 & -z_2\\
%z_2 & z_1
%\end{array}\right]$$


\section{Transformaciones 2D y matrices afines}

La pose de un robot en el plano, con respecto a una terna global, se puede escribir como
$\mathbf{x} = (x, y, \theta)$, donde $(x, y)$ es la posición del robot en el plano y $\theta$ es su orientación. La matriz de transformación homogénea que representa la pose $\mathbf{x} = (x, y, \theta)$ con respecto al origen de un sistema de coordenadas global está dada por
$$\mathbf{T(\mathbf{x})}=\left[\begin{array}{cc}
\mathbf{R}(\theta) & \mathbf{t}\\
\mathbf{0} & 1
\end{array}\right]$$
con $\mathbf{t}=[x, y]^T$ y $\mathbf{R}(\theta)$ la matriz de rotación en el plano por el ángulo en sentido antihorario $\theta$ dada por
$$\mathbf{R}(\theta)=\left[\begin{array}{cc}
\cos(\theta) & -\sin(\theta)\\
\sin(\theta) & \cos(\theta)
\end{array}\right]$$

\textbf{NB.} El ángulo $\theta$ es el ángulo subtendido entre el eje $\hat{x}$ del sistema de coordenadas global y la dirección longitudinal del robot.

Decimos que $\mathbf{T(\mathbf{x})}$ representa la pose $\mathbf{x}$ en el sentido que el producto a izquierda de dicha matriz por cualquier punto del plano codificado en el vector $\bar{\mathbf{r}} = (r_x, r_y, 1)^T$ resulta en un vector $\bar{\mathbf{w}} = (a, b, 1)^T$ que codifica el punto $(r_x,r_y)$ del plano en una coordenada $(a,b)$ referida a la pose de origen $\mathbf{x}$. Así, la matriz $\mathbf{T(\mathbf{x})}$ codifica una rotación antihoraria en $\theta$ \emph{seguida por} una traslación en $(x,y)$.

\bigskip


\textbf{1.} Estando el robot en la pose $\mathbf{r}_1 = (a, b, \alpha)^T$ se detecta un obstáculo $p$ en la posición $\mathbf{p}_{r1}=(p_{xr1},p_{yr1})^T$ con respecto a su propia terna de referencia. Usar la matriz $\mathbf{T}(\mathbf{r}_1)$ para calcular las coordenadas de $p$ con respecto a la terna global, $\mathbf{p}_g$.

Vemos que si la matriz $\mathbf{T}(\mathbf{r}_1)$ codifica una rotación en $\alpha$ y luego una traslación en $(a,b)$, basta con considerar el caso 
$$\mathbf{T}(\mathbf{r}_1) \left[\begin{array}{c}
0\\
0\\
1
\end{array}\right]=\left[\begin{array}{c}
a\\
b\\
1
\end{array}\right]$$
para observar que la matriz $\mathbf{T}(\mathbf{r}_1)$ puede considerarse como la matriz de cambio de coordenadas de un sistema referido al punto $\mathbf{r}_1$ hacia otro referido al origen. Por lo tanto notaremos dicha matriz $\mathbf{T}_{r1}^g=\mathbf{T}(\mathbf{r}_1)$, donde el subíndice fija el sistema de referencia inicial(en este caso centrado en $\mathbf{r}_1$) y el superíndice fija el sistema de referencia final $g$, por \emph{global}.

Por lo tanto si desde la pose $\mathbf{r}_1$ se detecta un obstáculo en las coordenadas  $\mathbf{p}_{r1}=(p_{xr1},p_{yr1})^T$, la posición de $p$ con respecto a la terna global será $\mathbf{p}_g$ que será codificada por el vector $\bar{\mathbf{p}_g}$ y se calculará como:

\begin{align}
\bar{\mathbf{p}_g} &= \mathbf{T}_{r1}^g \bar{\mathbf{p}_{r1}} \label{ec:r1-to-global}\\
&= \mathbf{T}(\mathbf{r}_1) \left[\begin{array}{c}
p_{xr1}\\
p_{yr1}\\
1
\end{array}\right] \nonumber \\
 &=  \left[\begin{array}{cc}
\mathbf{R}(\alpha) & (a,b)^T\\
\mathbf{0} & 1
\end{array}\right] \left[\begin{array}{c}
p_{xr1}\\
p_{yr1}\\
1
\end{array}\right] \nonumber \\
 &= \left[\begin{array}{c}
\mathbf{R}(\alpha) (p_{xr1},p_{yr1})^T + (a,b)^T\\
1
\end{array}\right]
 \end{align}

Vector que codifica la transformación esperada de aplicarle una rotación a las coordenadas locales en $\mathbf{r}_1$ en $\alpha$ y luego una traslación igual al origen del sistema de referencia, $\mathbf{r}_1$, obteniéndose la posición de $p$ en terna global $\mathbf{p}_g=\mathbf{R}(\alpha) (p_{xr1},p_{yr1})^T + (a,b)^T$.

\bigskip


\textbf{2.} Dadas las coordenadas de un obstáculo en la terna global $p_g$, ¿cómo pueden calcularse las coordenadas de dicho obstáculo que el robot observará en su propia terna?

Ahora el problema es el inverso: obtener la matriz de cambio de coordenadas del sistema de referencia global a $\mathbf{r}_1$, que notamos $\mathbf{T}^{r1}_g$.

Es decir buscamos una matriz $\mathbf{B}$ tal que

$$\bar{\mathbf{p}_{r1}} = \mathbf{B} \bar{\mathbf{p}_g}$$

Premultiplicando en la ecuación (\ref{ec:r1-to-global}) por la matriz inversa $(\mathbf{T}^g_{r1})^{-1}$ se obtiene

$$\bar{\mathbf{p}_{r1}} = (\mathbf{T}^g_{r1})^{-1} \bar{\mathbf{p}_g}$$

es decir, $\mathbf{T}^{r1}_g=(\mathbf{T}^g_{r1})^{-1}$. Por propiedades de las matrices de transformaciones afines y de las matrices de rotación proponemos la expresión de la matriz inversa como:

$$\mathbf{T}^{r1}_g = (\mathbf{T}^g_{r1})^{-1} = \left[\begin{array}{cc}
\mathbf{R}(\alpha)^T & -\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right]$$

la cual codifica una traslación en $-(a,b)^T$ \emph{y luego} una rotación en $-\alpha$.

Como prueba de la proposición anterior basta mostrar que la matriz inversa propuesta cumple $\mathbf{T}_{r1}^g = (\mathbf{T}_g^{r1})^{-1}$, i.e. $\mathbf{T}_{r1}^g\mathbf{T}_g^{r1} = \mathbb{I}d^{3x3}$.

\bigskip
$\mathbf{T}_{r1}^g\mathbf{T}_g^{r1} =  $

$$= \left[\begin{array}{cc}
\mathbf{R}(\alpha)^T & -\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right]
 \left[\begin{array}{cc}
\mathbf{R}(\alpha) & (a,b)^T\\
\mathbf{0} & 1
\end{array}\right] $$

$$=
\left[\begin{array}{cc}
\mathbf{R}(\alpha)^T\mathbf{R}(\alpha) & \mathbf{R}(\alpha)^T(a,b)^T-\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right]
$$

$$=\left[\begin{array}{cc}
\mathbb{I}d^{2x2} & 0 \\
 \mathbf{0} & 1
\end{array}\right]
=\mathbb{I}d^{3x3}
$$
\qed

Por lo tanto para obtener las coordenadas respecto a $\mathbf{r}_1$, basta con calcular

\begin{align}
\bar{\mathbf{p}_{r1}} &= \mathbf{T}_g^{r1} \bar{\mathbf{p}_g} \nonumber \\
&= \left[\begin{array}{cc}
\mathbf{R}(\alpha)^T & -\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right] \left[\begin{array}{c}
p_{xg}\\
p_{yg}\\
1
\end{array}\right] \nonumber \\
&=\left[\begin{array}{c}
\mathbf{R}(\alpha)^T(p_{xg},p_{yg})^T -\mathbf{R}(\alpha)^T(a,b)^T\\
1
\end{array}\right] \nonumber
\end{align}

vector que codifica las coordenadas de $p$ respecto a $\mathbf{r}_1$, $\mathbf{p}_{r1}=\mathbf{R}(\alpha)^T(p_{xg}-a,p_{yg}-b)^T$.

\bigskip


\textbf{3.} El robot se mueve a una nueva pose $\mathbf{r}_2 = (c , d , \beta )^T$ en la terna global. Encontrar la matriz de transformación $\mathbf{T}_{\mathbf{r}1}(\mathbf{r}2)$ que representa la nueva pose con respecto a $\mathbf{r}_1$.

Si $\mathbf{T}(\mathbf{x})$ representa la pose $\mathbf{x}$ con respecto a la terna global, y es también la matriz de cambio de coordenadas de una terna en $\mathbf{x}$ a otra en el origen, entonces $\mathbf{T}_{\mathbf{r}1}(\mathbf{r}2)$ es también la matriz de cambio de coordenadas de una terna en $\mathbf{r}_2$ a otra en $\mathbf{r}_1$, es decir $\mathbf{T}_{r2}^{r1}$. Por propiedades de las transformaciones afines, concatenar cambios de coordenadas es válido siempre que se tome un sistema de referencia intermedio en común, por ejemplo $\mathbf{T}_a^c = \mathbf{T}_b^c \mathbf{T}_a^b$:
\bigskip

$\mathbf{T}_{\mathbf{r}1}(\mathbf{r}2) = \mathbf{T}_{r2}^{r1}$
$= \mathbf{T}_{g}^{r1} \mathbf{T}_{r2}^g $

$$=  \left[\begin{array}{cc}
\mathbf{R}(\alpha)^T & -\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right] \left[\begin{array}{cc}
\mathbf{R}(\beta) & (c,d)^T\\
\mathbf{0} & 1
\end{array}\right]
$$
\begin{align}
&= \left[\begin{array}{cc}
\mathbf{R}(\alpha)^T\mathbf{R}(\beta) & \mathbf{R}(\alpha)^T(c,d)^T-\mathbf{R}(\alpha)^T(a,b)^T\\
\mathbf{0} & 1
\end{array}\right] \nonumber \\
&= \left[\begin{array}{cc}
\mathbf{R}(\beta-\alpha) & \mathbf{R}(\alpha)^T\left[(c,d)^T-(a,b)^T\right]\\
\mathbf{0} & 1
\end{array}\right] \nonumber
\end{align}

donde se aplicaron los resultados anteriores para $\mathbf{T}_{r2}^g$ y $\mathbf{T}_{g}^{r1}$.

\bigskip


\textbf{4.} Estando ahora el robot en la posición $\mathbf{r}_2$, ¿dónde está el obstáculo $p$ con respecto a la nueva terna local del robot?

Las coordenadas $\mathbf{p}_{r2}$ se obtienen de la representación vectorial $\bar{\mathbf{p}_{r2}}$, calculada como la transformación de terna $\mathbf{r}_1$ a $\mathbf{r}_2$ de las coordenadas de $p$ en terna $\mathbf{r}_1$, $\mathbf{p}_{r1}=(p_{xr1},p_{yr1})^T$. Por lo tanto

\bigskip

$\bar{\mathbf{p}_{r2}} = \mathbf{T}_{r1}^{r2} \bar{\mathbf{p}_{r1}}$
\begin{align}
&= \left[\begin{array}{cc}
\mathbf{R}(\alpha-\beta) & \mathbf{R}(\beta)^T\left[(a,b)^T-(c,d)^T\right] \\
\mathbf{0} & 1
\end{array}\right] \left[\begin{array}{c}
p_{xr1} \\
p_{yr1} \\
1
\end{array}\right] \nonumber \\
&=\left[\begin{array}{c}
\mathbf{R}(\beta)^T \left[\mathbf{R}(\alpha)(p_{xr1},p_{yr1})^T +(a,b)^T-(c,d)^T\ \right]\\
1
\end{array}\right] \nonumber
\end{align}

De lo cual se obtienen las coordenadas de $p$ en terna $\mathbf{r}_2$:
$\mathbf{p}_{r2} =\mathbf{R}(\beta)^T \left[\mathbf{R}(\alpha)(p_{xr1},p_{yr1})^T +(a,b)^T-(c,d)^T\ \right]$

\section{Sensado}
Un robot se encuentra en la pose $x = 1,0m, y = 0,5m, \theta = \pi / 4 $ , según una terna global.
Sobre el robot hay montado un LIDAR en la posición $x = 0,2m, y = 0,0m, \theta = \pi$ (con
respecto a la terna del cuerpo del robot). El sensor produce una lectura que se encuentra
en el archivo \texttt{laserscan.dat}. La primera medición se toma para el ángulo $\alpha = - \pi/2$ (según la terna del sensor) y la última se toma para el ángulo $\alpha = \pi/2$. El sensor tiene una apertura angular (FOV) de $\pi$ y todas las mediciones intermedias tienen una separación angular constante.
1. Graficar las mediciones en la terna de referencia del LIDAR.

2. ¿Cómo podrían explicarse las mediciones?

3. Usar las transformaciones homogéneas para calcular y graficar:

a) La posición del robot en la terna global.

b) La posición del LIDAR en la terna global.

c) Las mediciones en la terna global.

Primero se utiliza la función \texttt{polarplot(.)} para graficar las mediciones en coordenadas polares, como son provistas, en la terna del LIDAR.
Esta gráfica muestra que el robot se encuentra con un camino libre en dirección $0^o$, con un obstáculo macizo a su derecha a aproximadamente 1m cuyo lado corre paralelo a la dirección $0^o$, y un obstáculo en dicha dirección a aproximadamente 10m. A su izquierda se encuentra otro obstáculo paralelo al primero a aprox. 0.5m, pero que presenta cierta permeabilidad a las mediciones por las cuales se observa otro recinto cuyo límite se encuentra a unos 7m y presenta 4 obstáculos en el mismo.

Podría interpretarse entonces que el robot se encuentra en un pasillo que dobla a la derecha luego de unos 5m, y a su izquierda se encuentra delimitado por un vidrio ó un enrejado detrás del cual hay otro recinto.

Luego se calcula mediante los métodos desarrollados en la primera sección de este trabajo las transformaciones de coordenadas necesarias para graficar la posición del robot, su pose y las mediciones en la terna global.

Se adjunta al presente informe la publicación del código fuente, con la salida del mismo.



\section{Accionamiento diferencial}

Escribir una función en Octave/MATLAB que implementa la cinemática directa para un robot de accionamiento diferencial.

1. El encabezado de la función debe tener esta forma:
function [x\_n y\_n theta\_n] = diffdrive(x, y, theta, v l, v r, t, l)
donde x, y, theta es la pose del robot, v l y v r son las velocidades de la rueda izquierda y derecha, t es el intervalo de tiempo en movimiento y l es la distancia entre ruedas del robot. La salida de la función es una nueva pose del robot dada por x\_n, y\_n, theta\_n.

2. Comenzando en la pose $x = 1,5m, y = 2,0m, \theta = \pi /2$ , el robot ejecuta la siguiente secuencia de acciones:

a) $c1 = (v_l = 0,3m/s, v_r = 0,3m/s, t = 3s)$

b) $c2 = (v_l = 0,1m/s, v_r = -0,1m/s, t = 1s)$

c) $c3 = (v_l = 0,2m/s, v_r = 0,0m/s, t = 2s)$

Usar la función creada para calcular la pose del robot al ejecutar estas acciones si la distancia l entre ruedas del robot es 0,5m. Graficar el movimiento resultante.


Se escribió la función \texttt{diffdrive(.)}, la cual consiste en implementar directamente las ecuaciones de la cinemática directa del accionamiento diferencial, con la condición de que las velocidades de las ruedas $v_l$ y $v_r$ sean diferentes. De lo contrario el cálculo se simplifica ya que el movimiento claramente será en línea recta y no corresponde calcular ningún parámetro relacionado a la guiñada del robot.

Luego se utilizó dicha función para calcular la actualización sucesiva de poses del robot con un período de 0.1s, y se graficó la trayectoria en (x,y).

Se adjunta al presente informe la publicación del código fuente, con la salida del mismo.