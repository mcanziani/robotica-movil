%% 3. Accionamiento diferencial
% Escribir una función en Octave/MATLAB que implementa la cinemática 
% directa para un robot de accionamiento diferencial.
% 
% 1. El encabezado de la función debe tener esta forma:
% function [x\_n y\_n theta\_n] = diffdrive(x, y, theta, v l, v r, t, l)
% donde x, y, theta es la pose del robot, v l y v r son las velocidades de
% la rueda izquierda y derecha, t es el intervalo de tiempo en movimiento
% y l es la distancia entre ruedas del robot. La salida de la función es 
% una nueva pose del robot dada por x\_n, y\_n, theta\_n.
% 
% 2. Comenzando en la pose $x = 1,5m, y = 2,0m, \theta = \pi$ , el robot
% ejecuta la siguiente secuencia de acciones:
% 
% a) $c1 = (v_l = 0,3m/s, v_r = 0,3m/s, t = 3s)$
% 
% b) $c2 = (v_l = 0,1m/s, v_r = -0,1m/s, t = 1s)$
% 
% c) $c3 = (v_l = 0,2m/s, v_r = 0,0m/s, t = 2s)$
% 
% Usar la función creada para calcular la pose del robot al ejecutar
% estas acciones si la distancia l entre ruedas del robot es 0,5m. 
% Graficar el movimiento resultante.
function [x_n, y_n, theta_n] = diffdrive(x, y, theta, v_l, v_r, t, l)
RV=zeros(1,3);
% Si v_l y v_r son iguales, simplemente moverse en linea recta en la
% dirección theta a esa velocidad
if(v_l==v_r)
    w=0;
    RV(1)=x+t*v_l*cos(theta);
    RV(2)=y+t*v_l*sin(theta);
    RV(3)=theta;
else
    % Cuando v_l y v_r son diferentes hay un radio de curvatura y se aplica
    % las ecuaciones vistas de la cinemática directa
    r=(l/2)*(v_r+v_l)/(v_r-v_l);
    ICCx=x-r*sin(theta);
    ICCy=y+r*cos(theta);
    w=(v_r-v_l)/l;
    RV=[cos(w*t) -sin(w*t) 0;
         sin(w*t) cos(w*t)  0;
         0       0        1] * [x-ICCx;
                                y-ICCy;
                                theta ] + [ICCx;
                                           ICCy;
                                           w*t  ];
end %if                                  
% actualización de parámetros de salida
x_n=RV(1);
y_n=RV(2);
theta_n=RV(3);                            
end %function