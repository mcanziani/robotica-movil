%% 2. Sensado
% Un robot se encuentra en la pose x = 1,0m, y = 0,5m, \theta = \pi / 4,
% según una terna global. Sobre el robot hay montado un LIDAR en la 
% posición x = 0,2m, y = 0,0m, \theta = \pi (con respecto a la terna del
% cuerpo del robot). El sensor produce una lectura que se encuentra
% en el archivo \texttt{laserscan.dat}. La primera medición se toma para el
% ángulo $\alpha = - \pi/2$ (según la terna del sensor) y la última se toma
% para el ángulo $\alpha = \pi/2$. El sensor tiene una apertura angular 
% (FOV) de $\pi$ y todas las mediciones intermedias tienen una separación
% angular constante.
%
% 1. Graficar las mediciones en la terna de referencia del LIDAR.
% 
% 2. ¿Cómo podrían explicarse las mediciones?
% 
% 3. Usar las transformaciones homogéneas para calcular y graficar:
% 
% a) La posición del robot en la terna global.
% 
% b) La posición del LIDAR en la terna global.
% 
% c) Las mediciones en la terna global.

%% Input & gráfica mediciones en terna Lidar coord polares
clear all, close all
% carga de datos
scan = load('-ascii', '../laserscan.dat');
% generación de coordenadas del ángulo
angle = linspace(-pi/2, pi/2, size(scan,2));
% gráfica polar
h=figure(1);
polarplot(angle, scan,'. ');
title('Mediciones LIDAR en terna LIDAR polar')
rticks(1:2:11)
rticklabels({'1m','3m','5m','7m','9m','11m'})
set(h, 'Position', [0 0 700 700])

%% Condición inicial robot y lidar
% posición del robot en terna global
rRobotGlobal = [1, 0.5, pi/4];
% posición del Lidar en terna body del robot
rLidarBody = [0.2, 0, pi];

%% Transformaciones de coordenadas y refs
% Matriz de transformación de terna body a terna global
TbodyGlobal = [ cos(rRobotGlobal(3)),-sin(rRobotGlobal(3)),rRobotGlobal(1);
                sin(rRobotGlobal(3)),cos(rRobotGlobal(3)),rRobotGlobal(2);
                0,0,1 ];

% cálculo de la posición del Lidar en terna global
rLidarGlobal = zeros(1,3);
aux =  (TbodyGlobal * [rLidarBody(1); rLidarBody(2); 1]).';
rLidarGlobal(1:2) = aux(1:2);
clear aux
% Se actualiza el parámetro ángulo de la POSE del Lidar en terna global a
% partir de los ángulos de pose del robot en terna global y del lidar en
% terna robot
rLidarGlobal(3)=rLidarBody(3)+rRobotGlobal(3);

% paso a coordenadas cartesianas de los datos del Lidar
[x,y]=pol2cart(angle, scan);

% Matriz de transformación de terna lidar a terna global
TlidarGlobal = [cos(rLidarGlobal(3)),-sin(rLidarGlobal(3)),rLidarGlobal(1);
                sin(rLidarGlobal(3)),cos(rLidarGlobal(3)),rLidarGlobal(2);
                0,0,1 ];

% Transformación de las mediciones de terna lidar a global
aux = (TlidarGlobal * [x;y;ones(size(x))]).';
rMedicionesGlobal=aux(:,1:2);            
clear aux

% nueva gráfica
h=figure(2); hold on

% Origen
plot(0,0,'x ', 'Color', 'green', 'MarkerSize', 12)

% Mediciones
plot(rMedicionesGlobal(:,1),rMedicionesGlobal(:,2),'. ', 'Color', 'blue');

% Robot
drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0 );

drawArrow([rRobotGlobal(1) rRobotGlobal(1)*(1+cos(rRobotGlobal(3)))], ...
    [rRobotGlobal(2) rRobotGlobal(2)*(1+sin(rRobotGlobal(3)))]);

% Lidar
drawArrow([rLidarGlobal(1) rLidarGlobal(1)*(1+cos(rLidarGlobal(3)))], ...
    [rLidarGlobal(2) rLidarGlobal(2)*(1+sin(rLidarGlobal(3)))]);

% Posición origen de la terna lidar
aux=TlidarGlobal * [0;0;1];
plot(aux(1),aux(2),'x ', 'Color', 'red', 'MarkerSize', 12);
clear aux;

axis equal
legend('Origen','Mediciones Lidar','Pose Robot','Pose Lidar',...
    'Origen Terna Lidar', 'Location', 'northeast')
xlabel('x[m]')
ylabel('y[m]')
title('Mediciones LIDAR en terna global')
set(h, 'Position', [0 0 700 700])
