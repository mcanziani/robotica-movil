%% 3. Accionamiento diferencial
% Escribir una función en Octave/MATLAB que implementa la cinemática 
% directa para un robot de accionamiento diferencial.
% 
% 1. El encabezado de la función debe tener esta forma:
% function [x\_n y\_n theta\_n] = diffdrive(x, y, theta, v l, v r, t, l)
% donde x, y, theta es la pose del robot, v l y v r son las velocidades de
% la rueda izquierda y derecha, t es el intervalo de tiempo en movimiento
% y l es la distancia entre ruedas del robot. La salida de la función es 
% una nueva pose del robot dada por x\_n, y\_n, theta\_n.
% 
% 2. Comenzando en la pose $x = 1,5m, y = 2,0m, \theta = \pi /2$ , el robot
% ejecuta la siguiente secuencia de acciones:
% 
% a) $c1 = (v_l = 0,3m/s, v_r = 0,3m/s, t = 3s)$
% 
% b) $c2 = (v_l = 0,1m/s, v_r = -0,1m/s, t = 1s)$
% 
% c) $c3 = (v_l = 0,2m/s, v_r = 0,0m/s, t = 2s)$
% 
% Usar la función creada para calcular la pose del robot al ejecutar
% estas acciones si la distancia l entre ruedas del robot es 0,5m. 
% Graficar el movimiento resultante.

%%
clear all, close all

%% Condiciones iniciales
x_0=[1.5,2,pi/2];
l=0.5;

% Parámetros de la simulación
deltat=0.1;
tEnd=6;
t=0:deltat:tEnd;
x=zeros(length(t),1);
y=zeros(length(t),1);
heading=zeros(length(t),1);

%% c1
ti=0;
x(1)=x_0(1);
y(1)=x_0(2);
heading(1)=x_0(3);
v_l=0.3;
v_r=0.3;
tf=ti+3;
for n=1:tf/deltat
    [x(n+1),y(n+1),heading(n+1)]=diffdrive(x(n), y(n), heading(n), v_l, v_r, deltat, l);
end %for

%% c2
ti=3;
v_l=0.1;
v_r=-0.1;
tf=ti+1;
for n=(ti/deltat)+1:tf/deltat
    [x(n+1),y(n+1),heading(n+1)]=diffdrive(x(n), y(n), heading(n), v_l, v_r, deltat, l);
end %for

%% c3
ti=4;
v_l=0.2;
v_r=-0;
tf=ti+2;
for n=(ti/deltat)+1:tf/deltat
    [x(n+1),y(n+1),heading(n+1)]=diffdrive(x(n), y(n), heading(n), v_l, v_r, deltat, l);
end %for

%% gráfica de la trayectoria

h= figure(3); hold on
plot(x,y, '.-')
plot(x(1),y(1), 'o ', 'MarkerSize', 12, 'color', 'blue')
plot(x(length(x)),y(length(y)), 'x ', 'MarkerSize', 12, 'color', 'red')
legend('Trayectoria','Inicio', 'Fin')
title('Trayectoria del móvil')
xlabel('x[m]')
ylabel('y[m]')

grid on
axis equal
hold off
set(h, 'Position', [0 0 700 700])