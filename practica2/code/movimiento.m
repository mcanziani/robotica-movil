function [ xtp1 ] = movimiento( xt , ut, alpha )
%MOVIMIENTO 

[a,b]=size(xt);
assert(a==3 && b==1,'"xt" must be a 3x1 matrix')

[a,b]=size(ut);
assert(a==3 && b==1,'"ut" must be a 3x1 matrix')

[a,b]=size(alpha);
assert(a==4 && b==1,'"ut" must be a 4x1 matrix')

dr1=ut(1)+randn*(alpha(1)*abs(ut(1))+alpha(2)*ut(3));
dt=ut(3)+randn*(alpha(3)*ut(3)+alpha(4)*(abs(ut(1))+abs(ut(2))));
dr2=ut(2)+randn*(alpha(1)*abs(ut(2))+alpha(2)*ut(3));

xtp1=zeros(3,1);
xtp1(1,1)=xt(1)+dt*cos(xt(3)+dr1);
xtp1(2,1)=xt(2)+dt*sin(xt(3)+dr1);
xtp1(3,1)=xt(3)+dr1+dr2;


end

