function [ rv ] = randn_3(  )
%RANDN_1 Generar una muestra de la distribución Normal(0,1)
% Se utliliza el método de Box-Muller
u1=rand; u2=rand;
rv=cos(2*pi*u1)*sqrt(-2*log(u2));
end

