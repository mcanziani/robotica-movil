function [ rBel ] = updateBel( belief, movement )
%UPDATEBEL updates belief with one step forward or backwards dep. on
%movement

%check
[a,~]=size(belief);
assert(a==1,'belief must be a row vector');
assert(sum(belief)==1,...
'belief must be a valid probability discrete mass function (sum(belief)=1)')

%se extiende el belief dos celdas en ambos límites
eBelief = [0 0 belief 0 0];
posterior=[0.25, 0.5, 0.25];
switch movement
    case 1 %hacia adelante
        %filter(b,1,d) hace la convolución d*b y devuelve un vector de
        %largo igual a d
        rBel = filter(posterior,1,eBelief);
    case -1 %hacia atrás
        rBel = fliplr(filter(posterior,1,fliplr(eBelief)));
    otherwise
        error('Invalid movement (1=forward,-1=backward)')
end%switch

%clipping de las masas de probabilidad en celdas extendidas
lenB=length(rBel);
rBel(3) = rBel(1)+rBel(2)+rBel(3);
rBel(lenB-2)=rBel(lenB-2)+rBel(lenB-1)+rBel(lenB);
rBel = rBel(3:lenB-2);

end

