function [ rv ] = randn_1(  )
%RANDN_1 Generar una muestra de la distribución Normal(0,1)
% Se utiliza la suma de N variables Uniforme(-1,1)
N=12;
rv=sum((rand(N,1)-0.5)*sqrt(12))/sqrt(N);
end

