%% movimiento_evaluation.m
clear variables, close all


x1=[2;4;0];
u1=[pi/2;0;1];
alpha=[0.1;0.1;0.01;0.01];

m=5e3;

%%
x2=zeros(3,m);
for i=1:m
    x2(:,i)=movimiento(x1,u1,alpha);
end%for


%%
figure(1)
hold on
plot(x1(1),x1(2), 'o ', 'color', 'blue');
for i=1:m
    plot(x2(1,i),x2(2,i),'. ', 'color', 'black');
end%for    
xlim([0 4])
ylim([3 6])
xlabel('x[m]')
ylabel('y[m]')
