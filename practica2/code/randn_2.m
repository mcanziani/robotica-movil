function [ rv ] = randn_2(  )
%RANDN_1 Generar una muestra de la distribución Normal(0,1)
% Se utiliza muestreo con rechazo
while 1 
    % se muestrea rv de una Uniforme(-5,5). El soporte se reduce a [-5,5]
    rv = (2*rand-1)*5;
    % se muestrea c de una Uniforme(0,1/2pi) que es el máximo de la N(0,1)
    %c = rand*(1/(2*pi));
    % se escala todo por 2pi
    % se compara 2pi*phi(rv,1) con 2pi*c
    if exp(-(rv^2)/2) > rand % i.e. si f(x) > c entonces conservar muestra
        break %while
    end % si f(x) <= c entonces rechazar la muestra y tomar otra        
end%while
end

