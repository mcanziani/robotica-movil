%% discreto.m
clear variables, close all

%simulation time max
tf=12;
%state space 
totalLength = 20;

%robot inputs
movement=zeros(1,tf);
movement(1,1:9)=ones(1,9);
movement(1,10:12)=-ones(1,3);

%belief
bel=zeros(tf+1,totalLength);
bel(1,1:20)=[zeros(1,10), ones(1,1), zeros(1,9)];

for i=1:tf
    bel(i+1,:) = updateBel( bel(i,:), movement(i) );
end%for


[xx, yy] = meshgrid(1:tf+1,1:totalLength);
plot3(xx,yy,bel.','*-')