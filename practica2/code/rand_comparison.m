%% rand_comparison.m
clear variables, close all
% Comparación de diferentes métodos de muestreo de variables normales

% Sample size
%
m = 1e3;
n = 1e3;

%% Tests

tStart = tic;
for i=1:m
    m_randn(0,1,n,1);
end%for
t1 = toc(tStart);

tStart = tic;
for i=1:m
    m_randn(0,1,n,2);
end%for
t2 = toc(tStart);

tStart = tic;
for i=1:m
    m_randn(0,1,n,3);
end%for
t3 = toc(tStart);

tStart = tic;
for i=1:m
    normrnd(0,1,n,1);
end%for
t4 = toc(tStart);

tStart = tic;
for i=1:m
    randn(n,1);
end%for
t5 = toc(tStart);

clear ans i tStart
%% Data output

fprintf('Generación de %i vectores de %i muestras con el método 1: \t \t %f s\n',m,n,t1)
fprintf('Generación de %i vectores de %i muestras con el método 2: \t \t %f s\n',m,n,t2)
fprintf('Generación de %i vectores de %i muestras con el método 3: \t \t %f s\n',m,n,t3)
fprintf('Generación de %i vectores de %i muestras con la función normrnd: \t %f s\n',m,n,t4)
fprintf('Generación de %i vectores de %i muestras con la función randn: \t %f s\n',m,n,t5)