% Martín Canziani
%   FIUBA 1c2018 - Robótica Móvil
function weight = measurement_model(z, x, l)
    % Computes the observation likelihood of all particles.
    %
    % The employed sensor model is range only.
    %
    % z: set of landmark observations. Each observation contains the id of the landmark observed in z(i).id and the measured range in z(i).range.
    % x: set of current particles
    % l: map of the environment composed of all landmarks
    sigma = 0.2;
    weight = ones(size(x, 1), 1);

    if size(z, 2) == 0
        return
    end
    
    %para cada partícula
    for i = 1:size(x, 1)
        %landmark_position = [l(z(i).id).x, l(z(i).id).y];
        %measurement_range = [z(i).range];

        %% compute weight
        % El cálculo de la p(z|x,l) será
        %xr=x(i,1);
        %yr=x(i,2);
        %xl=l(z(i).id).x;
        %yl=l(z(i).id).y;
        %a = sqrt( (xr-xl)^2 + (yr-yl)^2 ) - z(i).range; 
        %weight = 1 /sigma sqrt(2pi) * exp(-((a)^2) / (2*sigma^2));
        
        % reemplazo para no tener variables intermedias en un ciclo
        
        % Escalo todos los pesos por el factor
        %(1/(sigma*sqrt(2*pi)))^-1

        % para cada medición
        for j = 1:size(z,2)
            % evaluar pdf normal y combinar mediciones independientes 
            % (multiplicar weights)
            weight(i,1)=weight(i,1) * ...
                    exp(- ( ( sqrt( (x(i,1)-l(z(j).id).x)^2 + ...
                                    (x(i,2)-l(z(j).id).y)^2   ) - ...
                             z(j).range ) ...
                           ^2  ) ...
                        / (2*sigma^2) );  
            
        end%for j
    end%for i
    % Escalar todos los pesos por el mismo factor no altera el resultado ya
    % que se normalizarán siempre
    % weight = weight ./ size(z, 2);
    
    % conviene normalizar acá para que la salida de la función sea una fdp.
    % No lo hago para no modificar particle_filter.m
end

