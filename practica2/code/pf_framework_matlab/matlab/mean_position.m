% Martín Canziani
%   FIUBA 1c2018 - Robótica Móvil
function mean_pos = mean_position(particles, weights)
    % Returns a single estimate of filter state based on the particle cloud.
    %
    % particles (M x 3): set of M particles to sample from. Each row contains a state hypothesis of dimension 3 (x, y, theta).
    % weights (M x 1): weights of the particles. Each row contains a weight.

    % initialize
    %mean_pos = zeros(size(particles(1,:)));

    %%
    mean_pos=sum((repmat(weights, 1,3).*particles));
end
