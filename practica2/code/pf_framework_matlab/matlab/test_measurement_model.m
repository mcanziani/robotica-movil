% Martín Canziani
%   FIUBA 1c2018 - Robótica Móvil
% Read world data, i.e. landmarks
fprintf('Reading world data ...');
landmarks = read_world('../data/world.dat');
fprintf(' done\n\n');
disp('landmark 1 is:');
disp(landmarks(1));


particles = [0 0 0; 0.1 0.1 0; 0 0.1 0; -0.2 0.1 0.5];
z = struct;
z(1).id = 1;
z(1).range = 2.3;
%Problem: this only tests the case for a single landmark
%z(2).id = 1;
%z(2).range = 2.3;
likelihoods = measurement_model(z, particles, landmarks);
normalized_likelihoods = likelihoods' ./ sum(likelihoods)
disp('expecting normalized_likelihoods = 0.28283   0.18268   0.25808   0.27641');


% verify output
