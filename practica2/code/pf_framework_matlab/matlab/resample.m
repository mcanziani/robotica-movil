% Martín Canziani
%   FIUBA 1c2018 - Robótica Móvil
function new_particles = resample(particles, weights)
    % Returns a new set of particles obtained by performing
    % stochastic universal sampling.
    %
    % particles (M x D): set of M particles to sample from. Each row contains a state hypothesis of dimension D.
    % weights (M x 1): weights of the particles. Each row contains a weight.
    
    % validción
    assert ( size(weights,2) == 1, 'Weights must be a column vector');
    M = size(particles, 1);
    assert( M==size(weights,1) , 'Particle number and weight number must be equal');
    % reserva de memoria
    new_particles = zeros(size(particles));

    %%
    
    % tril(ones(n)) arma una matriz triangular inferior de unos y ceros
    % premultiplicando por el vector weights normalizado da un vector
    % c = [w1; w1+w2; w1+w2+w3; ...]
    c = tril(ones(M)) * (weights / sum(weights) );
    % inicializo un contador i
    i = 1;
    % inicializo el primer "tiro" en la rueda
    u1 = rand/M;
    
    %recorro el vector new_particles
    for j=1:M
        while (u1+((j-1)/M)) > c(i) %si el tiro cayó después de c(i)
        i = i+1; % pasar al siguiente c(i)
        if i>M, i=1; end%if % esto es para volver a cero cuando se terminan los c(i)
        end% while
    new_particles(j,:)=particles(i,:); % agregar la partícula correspondiente a c(i)
    end%for j
    %fprintf('Resampled %i particles\n', M) %debug
end
