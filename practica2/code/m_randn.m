function [ rv ] = m_randn( mu, sigma, n, method )
%M_RANDN Generar n muestras de una distribución Normal(mu, sigma) según
%uno de los tres métodos

% según el parámetro method, se elige la función correspondiente para
% generar una variable Normal(0,1) y se la escala por mu y sigma para 
% obtener una variable Normal(mu,sigma).
% Importante: La varianza es igual a sigma^2. A veces las distribuciones se
% notan como Normal(mu,sigma) y a veces como Normal(mu,sigma^2), es
% imporante establecer una convención.
switch method
    case 1
    rv = zeros(n,1);
    for i=1:n
        rv(i)=sigma*randn_1+mu;
    end%for
    case 2
    rv = zeros(n,1);
    for i=1:n
        rv(i)=sigma*randn_2+mu;
    end%for
    case 3
    rv = zeros(n,1);
    for i=1:n
        rv(i)=sigma*randn_3+mu;
    end%for   

    otherwise
        error('Invalid method');
end
